terraform {
  backend "s3" {
    bucket         = "orange-project-06-02-22"
    key            = "global/s3/terraform.tfstate"
    region         = "eu-west-2"
    dynamodb_table = "terraform-lock-orange"
    encrypt        = true
  }
}