resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.javaapp.id

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-gateway" })
  )
}
resource "aws_route_table" "internet" {
  vpc_id = aws_vpc.javaapp.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-route_table" })
  )
}

resource "aws_subnet" "javaapp-public-subnet-1" {
  vpc_id                  = aws_vpc.javaapp.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "eu-west-2a"
  map_public_ip_on_launch = true

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-public_subnet_1" })
  )
}

resource "aws_route_table_association" "public-subnet-1" {
  subnet_id      = aws_subnet.javaapp-public-subnet-1.id
  route_table_id = aws_route_table.internet.id
}

resource "aws_subnet" "javaapp-public-subnet-2" {
  vpc_id                  = aws_vpc.javaapp.id
  cidr_block              = "10.0.2.0/24"
  availability_zone       = "eu-west-2b"
  map_public_ip_on_launch = true

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-public_subnet_2" })
  )
}

resource "aws_route_table_association" "public-subnet-2" {
  subnet_id      = aws_subnet.javaapp-public-subnet-2.id
  route_table_id = aws_route_table.internet.id
}