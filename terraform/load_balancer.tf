resource "aws_lb" "javaapp" {
  internal                   = false
  load_balancer_type         = "application"
  subnets                    = [aws_subnet.javaapp-public-subnet-1.id, aws_subnet.javaapp-public-subnet-2.id]
  security_groups            = [aws_security_group.load_balancer.id]
  enable_deletion_protection = false

  tags = {
    Name = "${local.prefix}-load_balancer"
  }
}

resource "aws_lb_target_group" "javaapp" {
  name        = "javaapp-lb-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = aws_vpc.javaapp.id

  health_check {
    path                = "/"
    port                = "80"
    protocol            = "HTTP"
    healthy_threshold   = 3
    unhealthy_threshold = 3
    timeout             = 5
    interval            = 20
  }

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-lb_target_group" })
  )
}

resource "aws_lb_listener" "javaapp" {
  load_balancer_arn = aws_lb.javaapp.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.javaapp.arn
  }
}

resource "aws_lb_listener" "javaappHTTPS" {
  load_balancer_arn = aws_lb.javaapp.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "arn:aws:acm:eu-west-2:782874292641:certificate/5f160e8f-ccf8-4133-a67d-35022309f14c"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.javaapp.arn
  }
}