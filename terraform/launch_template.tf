resource "aws_launch_template" "javaapp_launch_template" {
  name                                 = "${local.prefix}-launch_template"
  image_id                             = "ami-0c638babe6d7db4be"
  instance_initiated_shutdown_behavior = "terminate"
  instance_type                        = "t2.micro"
  key_name                             = "javaapp"
  vpc_security_group_ids               = [aws_security_group.java_web.id]
  iam_instance_profile {
    name = aws_iam_instance_profile.ec2_profile.name
  }

  user_data = filebase64("s3_copy.sh")
  tag_specifications {
    resource_type = "instance"
    tags = merge(
      local.common_tags,
      tomap({ "Name" = "${local.prefix}-web_server" })
    )
  }
}

resource "aws_autoscaling_group" "javaapp" {
  name                = "${local.prefix}-auto_scaling_group"
  vpc_zone_identifier = [aws_subnet.javaapp-public-subnet-1.id, aws_subnet.javaapp-public-subnet-2.id]
  desired_capacity    = 2
  max_size            = 4
  min_size            = 2

  launch_template {
    id = aws_launch_template.javaapp_launch_template.id
  }

  lifecycle {
    ignore_changes = [load_balancers, target_group_arns]
  }
}

resource "aws_autoscaling_attachment" "javaapp_asg_attachment" {
  autoscaling_group_name = aws_autoscaling_group.javaapp.id
  alb_target_group_arn   = aws_lb_target_group.javaapp.arn
}

resource "aws_autoscaling_policy" "javaapp" {
  name                   = "${local.prefix}-autoscaling_policy}"
  policy_type            = "TargetTrackingScaling"
  adjustment_type        = "ChangeInCapacity"
  autoscaling_group_name = aws_autoscaling_group.javaapp.name

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 50.0
  }
}
