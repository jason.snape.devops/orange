resource "aws_route53_record" "www" {
  zone_id = "Z0901631194SVKKGOF62S"
  name    = "jasonsnape.co.uk"
  type    = "A"

  alias {
    name                   = aws_lb.javaapp.dns_name
    zone_id                = aws_lb.javaapp.zone_id
    evaluate_target_health = true
  }
}
