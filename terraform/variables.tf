variable "prefix" {
  default = "java-project-1"
}
variable "project" {
  default = "Orange project"
}
variable "contact" {
  default = "admin@my-java-project.com"
}